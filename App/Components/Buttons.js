import React from 'react';
import {Text, View} from 'react-native';
import {Button} from 'react-native-elements';
import constants from '../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../Utility/index';
const {strings, colors, fonts, urls, PATH} = constants;
const Buttons = ({navigation, title, click}) => {
  return (
    <Button
      title={title}
      type="clear"
      containerStyle={{}}
      titleStyle={{
        color: constants.white_Colors,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        alignSelf: 'center',
      }}
      buttonStyle={{
        backgroundColor: constants.title_Colors,
        width: wp('80%'),

        alignItems: 'center',
        justifyContent: 'center',
        height: hp('8%'),
        borderRadius: 50,
        alignSelf: 'center',
      }}
      raised={true}
      onPress={click}
    />
  );
};

export default Buttons;
