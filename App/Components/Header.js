import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {Button} from 'react-native-elements';
import constants from '../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../Utility/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
const {strings, colors, fonts, urls, PATH} = constants;
const Header = ({
  navigation,
  title,
  click,
  showPlus,
  route,
  showDot,
  dotPlus,
  iconPress
}) => {
  return (
    <View>
      <View
        style={{
          ...Platform.select({
            ios: {
              marginTop: hp('5%'),
            },
            android: {
              marginTop: hp('3%'),
            },
          }),
          // padding: wp('5%'),
          flexDirection: 'row',
          justifyContent: 'space-between',
          backgroundColor:constants.white_Colors,
          marginBottom:hp('2%')
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignSelf: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={  showDot==true?iconPress:() => navigation.goBack()}>
            <View
              style={{}}>

                {showDot==true?
                  <Icon
                  name={'bars'}
                  size={25}
                  color={constants.title_Colors}
                  style={{
                    alignSelf: 'center',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft:wp('5%')
                  }}
                />:
                <Icon
                name={''}
                size={25}
                color={constants.title_Colors}
                style={{
                  alignSelf: 'center',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft:wp('5%')
                }}
              /> 
             }
            </View>
          </TouchableOpacity>
         
        </View>
        <Text
            style={{ fontWeight: 'bold', fontSize: 25,color:constants.title_Colors}}>
            {title}
          </Text>
          <Text
            style={{fontWeight: 'bold', fontSize: 17,marginLeft:hp('5%')}}>
           
          </Text>
      </View>

    
    </View>
  );
};

export default Header;
