const colors = {
  title_Colors: 'rgba(83, 178, 232, 1)',
  white_Colors: 'white',
  grey_Background: '#E8E8E8',
  grey_Text: 'grey',
  black_Text: 'black',
  facebook:"#3b5998",
  trasparent_Color: '#fff0',
  textHeading_Color: '#383E56',
  light_Grey: '#bfbfbf',
};

export default colors;
