import React, { useState,useEffect } from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import constants from '../../../Constants/Colors';
import * as Utility from "../../../Utility/index"
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
import Logo from '../../../Components/Logo';
import InputField from '../../../Components/InputField';
import Button from '../../../Components/Buttons';
import styles from './style';
import { Get_User } from '../../../Redux/Action';
import {useDispatch, useSelector} from 'react-redux';
import Loader from '../../../Components/Loader';
import Header from "../../../Components/Header"
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
const { strings, colors, fonts, urls, PATH } = constants;
const UserSetting = ({ navigation, title }) => {
const dispatch=useDispatch()







  const [securityTxtStatus, setSecurityStatus] = useState(true);
  const [securityTxtStatus2, setSecurityStatus2] = useState(true);
  const [selectedImage,setSelectedImage]=useState('')
  const [email, Setemail] = useState('')
  const [loading, setLoading] = useState(false);
  const [password, SetPassword] = useState('');
  const [ConfirmPassword, SetConfirmPassword] = useState('')
  const [name, Setname] = useState('')
  useEffect(() => {
  
    retrieveData();
  
}, []);
const retrieveData = async() => {
  let id=await Utility.getFromLocalStorge('userId')
  setLoading(true)
let res=await dispatch(Get_User(id))
    // navigation.navigate('SwiperScreen')
    if(res.statusCode==200){
      setLoading(false)
      Setname(res.data.firstName)
      Setemail(res.data.email)
    }
    else{
      setLoading(false)
      Alert.alert('SomeThing Went Wrong')
    }
  
};

  const Register = async () => {
    if (Utility.isFieldEmpty(
      name && email.trim() && password && ConfirmPassword
    )) {
      return (Alert.alert('Please fill all the fields'))
    }
    else if (Utility.isValidEmail(email.trim())) {
      return Alert.alert('Please enter valid email');
    }
    else if (Utility.isValidComparedPassword(password, ConfirmPassword)) {
      return Alert.alert('Password miss match')
    }

    else {
      let body = {
        email: email.trim(),
        password: password,
        firstName: name,
        type:'',
        role:'parent'
      }
      setLoading(true);
      let res = await dispatch(Create(body));
      if (res.statusCode==200) {
        setLoading(false);
        // console.log("email===>",res.email[0])


        setLoading(false);
        Alert.alert(
          '',
          res.message,
          [{text: 'Ok', onPress: () => navigation.navigate('Login')}],
          { 
            cancelable: false
          },
        );

      }
    
      else {
        setLoading(false);
     Alert.alert(res.error)
      }
    }
  }



  const launchImageLibrarys = () => {
    let options = {
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    };
    launchImageLibrary(options, (response) => {
        console.log('Response = ', response);

        if (response.didCancel) {
            console.log('User cancelled image picker');
        } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
            alert(response.customButton);
        } else {
            console.log("check response uri", response.uri)
          setSelectedImage(response.assets[0].uri)
        }
    });

}

  return (
    <View style={{ flex: 1 ,backgroundColor:'white'}}>
      <Loader isLoading={loading}></Loader>
      <Header title={'User Profile'}></Header>
      <ScrollView>
        <TouchableOpacity onPress={()=>launchImageLibrarys()}>
          <Image source={selectedImage == '' ? require('../../../Assets/avtar.png') : { uri: selectedImage }}
            style={{ height: 120, width: 120, alignSelf: "center", marginTop: hp('5%'), borderRadius: 50 }}></Image>
</TouchableOpacity>

          <InputField
            placeHolder={'User Name'}
            value={name}
            inputonChangeText={name => Setname(name)}
          ></InputField>

          <InputField
            placeHolder={'Email'}

            autoCapitalization="none"
            value={email}
            editable={false}
            inputonChangeText={email => Setemail(email)}
          ></InputField>
 <View style={{marginTop:hp('1%')}}>
<InputField
            placeHolder={'Phone'}

            autoCapitalization="none"
            value={'Phone Number'}
          ></InputField>

</View>


       
          {/* <Button title={'Update'} click={() => Register()}></Button> */}
      </ScrollView>
    </View>
  );
};

export default UserSetting;
