import React from 'react';
import { Text, View,FlatList,TouchableOpacity ,Alert} from 'react-native';
import  Header  from '../../../Components/Header';
import { heightPercentageToDP, widthPercentageToDP } from '../../../Utility';
import Icon from 'react-native-vector-icons/FontAwesome';
import constants from '../../../Constants/Colors';
import * as Utility from "../../../Utility/index"
const PrivacyHome = ({navigation}) => {

  const goToRoute=async(value)=>{
    if(value.route=='Logout'){
   await   Signout()
    }
    else{
navigation.navigate(value.route)

    }
  }


  const Signout = async () => {
    Alert.alert(
      '',

      'Are you sure want to sign out?',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Button Pressed'),
          style: 'cancel',
        },

        {text: 'Sign out', onPress: () => removeAuth()},
      ],
    );
  };
const removeAuth=async()=>{
await Utility.removeAuthKey('token')
await Utility.removeAuthKey('userId')
navigation.navigate('Login')

}

  return (
    <View style={{  }}>
     

      <FlatList  
                    data={[  
                        {key: 'User Setting',route:'UserSetting'},{key: 'Privacy Policy',route:'PrivacyPolicy'}, {key: 'Contact US',route:'ContactUs'},{key: 'Term & Condition',route:'TermCondition'},  
                    {key:"Logout",route:'Logout'}
                    ]}  
                    renderItem={({item}) =>  
                    <TouchableOpacity onPress={()=>goToRoute(item)}>
                    <View style={{flexDirection:"row",justifyContent:"space-between",width:widthPercentageToDP('80%'),alignSelf:"center",marginTop:heightPercentageToDP('5%')}}>
                       <Text style={{textAlign:"center",marginTop:heightPercentageToDP('5%')}}>
       {item.key}
       </Text>
       <Icon
                  name={'angle-right'}
                  size={28}
                  color={constants.title_Colors}
                  style={{
                    alignSelf: 'center',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop:heightPercentageToDP('5%')
                  }}
                />
                      </View>
                      </TouchableOpacity>
                          // angle-right
                          }  
                />  
    </View>
  );
}

export default PrivacyHome;