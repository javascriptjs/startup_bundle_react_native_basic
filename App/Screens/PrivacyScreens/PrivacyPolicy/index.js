import React,{useEffect} from 'react';
import { BackHandler } from 'react-native';
import { WebView } from 'react-native-webview';
const PrivacyPolicy = ({navigation}) => {
  // useEffect(() => {
  //   BackHandler.addEventListener("hardwareBackPress", navigation.goBack());

  //   return () => {
  //     BackHandler.removeEventListener("hardwareBackPress", navigation.goBack());
  //   };
  // }, []);

  function handleBackButtonClick() {
    // navigation.goBack();
    console.log("checkback ")
navigation.goBack()
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);
  return (
    <WebView source={{ uri: 'http://13.54.226.124:4604/privacy' }} />
  );
}

export default PrivacyPolicy;