import React ,{useEffect}from 'react';
import { WebView, } from 'react-native-webview';
import {View, Text, Image, ImageBackground, BackHandler} from 'react-native';

const TermCondition = ({navigation}) => {
  function handleBackButtonClick() {
    // navigation.goBack();
    console.log("checkback ")
navigation.goBack()
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);
  return (
    <WebView source={{ uri: 'http://13.54.226.124:4604/terms' }} />
  );
}

export default TermCondition;