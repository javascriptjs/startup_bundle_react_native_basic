import React, { useEffect, useState } from 'react';
import {View, Text, Image, ImageBackground, BackHandler} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ContantUs = ({ navigation }) => {

  const [tab, setTab] = useState('3')


  function handleBackButtonClick() {
    // navigation.goBack();
    console.log("checkback ")
navigation.goBack()
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);
  return (
    <View style={{ flex: 1 }}>
      <View style={{ backgroundColor: 'pink', width: wp('20%'), alignSelf: 'flex-end', height: hp('100%') }}>

        {tab == '1' ?
          <ImageBackground source={require('../../../Assets/white.png')} style={{ height: hp('20%'), width: wp('20%'), resizeMode: 'contain', }}>
            <Text style={{ color: 'black', fontSize: 15, transform: [{ rotate: '90deg' }], top: 90 }}>Speech Tools</Text>

          </ImageBackground> :




          <TouchableOpacity onPress={() => setTab('1')}>

            <View style={{ height: hp('30%'), width: wp('20%'), backgroundColor: 'pink', alignSelf: 'flex-end' }}>
              <Text style={{ color: 'black', fontSize: 15, transform: [{ rotate: '90deg' }], top: 100, }}>{'Speech Tools'}</Text>
            </View>
          </TouchableOpacity>
        }




        {tab == '2' ?
          <ImageBackground source={require('../../../Assets/white.png')} style={{ height: hp('20%'), width: wp('20%'), resizeMode: 'contain', }}>
            <Text style={{ color: 'black', fontSize: 15, transform: [{ rotate: '90deg' }], top: 90 }}>Chat</Text>

          </ImageBackground> :

          <TouchableOpacity onPress={() => setTab('2')}>
            <View style={{ height: hp('20%'), width: wp('20%'), backgroundColor: 'pink', alignSelf: 'flex-end' }}>
              <Text style={{ color: 'black', fontSize: 15, transform: [{ rotate: '90deg' }], top: 100 }}>Chat</Text>
            </View>
          </TouchableOpacity>

        }




        {tab == '3' ?



          <ImageBackground source={require('../../../Assets/white.png')} style={{ height: hp('20%'), width: wp('20%'), resizeMode: 'contain', }}>
            <Text style={{ color: 'black', fontSize: 15, transform: [{ rotate: '90deg' }], top: 90 }}>Leasons</Text>

          </ImageBackground> :
          <TouchableOpacity onPress={() => setTab('3')}>

            <View style={{ height: hp('20%'), width: wp('20%'), backgroundColor: 'pink', alignSelf: 'flex-end' }}>
              <Text style={{ color: 'black', fontSize: 15, transform: [{ rotate: '90deg' }], top: 100 }}>Leasons</Text>
            </View>
          </TouchableOpacity>
        }


        {tab == '4' ?
          <ImageBackground source={require('../../../Assets/white.png')} style={{ height: hp('20%'), width: wp('20%'), resizeMode: 'contain', }}>
            <Text style={{ color: 'black', fontSize: 15, transform: [{ rotate: '90deg' }], top: 90 }}>Challange</Text>

          </ImageBackground> :


          <TouchableOpacity onPress={() => setTab('4')}>

            <View style={{ height: hp('20%'), width: wp('20%'), backgroundColor: 'pink', alignSelf: 'flex-end' }}>
              <Text style={{ color: 'black', fontSize: 15, transform: [{ rotate: '90deg' }], top: 100 }}>Challange</Text>
            </View>
          </TouchableOpacity>
        }

      </View>
    </View>
  );
};
export default ContantUs;
