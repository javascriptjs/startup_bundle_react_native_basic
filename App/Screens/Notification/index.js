import React ,{useState}from 'react';
import { Text, View,ScrollView,FlatList,TouchableOpacity ,Alert ,StyleSheet} from 'react-native';
import {heightPercentageToDP as hp,widthPercentageToDP as wp} from "../../Utility"

import * as Utility from "../../Utility/index"
import { SwipeListView,SwipeRow } from 'react-native-swipe-list-view';
import Header from "../../Components/Header"
import constants from '../../Constants/Colors';

const Notification = ({navigation}) => {
const [visible,setVisible]=useState(false)
const [data, setData] = useState(
  [{ text: 'Swipe Row #1', left: 'Left 1', right: 'Right 1', key: '1' },
  { text: 'Swipe Row #2', left: 'Left 2', right: 'Right 2', key: '2' },
  { text: 'Swipe Row #3', left: 'Left 3', right: 'Right 3', key: '3' },
  ]
)
const Signout = async () => {
  Alert.alert(
    '',

    'Are you sure Delete notification ',
    [
      {
        text: 'No',
        onPress: () => console.log('Cancel Button Pressed'),
        style: 'cancel',
      },

      {text: 'Yes', onPress: () => console.log('Cancel Button Pressed')},
    ],
  );
};
  return (
    <View style={{ flex: 1,paddingLeft:wp('5%'),paddingRight:wp('5%')  }}>

{/* <Header navigation={navigation}   title={'Notification'}> </Header> */}

<Text
            style={{ fontWeight: 'bold', fontSize: 25,color:constants.title_Colors,textAlign:"center",marginTop:hp('2.5%')}}>
            {'Notification'}
          </Text>

    <ScrollView >
      <Text style={{color:'rgba(73, 73, 73, 1)',fontSize:23,marginTop:hp('3%'),width:wp('80%'),alignSelf:"center",fontWeight:'bold'}}>Notifications (3)</Text>
        <View style={{
          width: wp('90%'), alignSelf: "center", marginTop: hp('5%'),

  }}>
    <View>
    <View style={{flexDirection:"row",marginTop:hp('5%'),marginLeft:wp('5%')}}>
     <Text style={{fontSize:17,color:'rgba(73, 73, 73, 1)',}}>Alert</Text>
     <Text style={{fontSize:17,color:'red',}}>  . 3New</Text>

     </View>

     <View style={styles.container}>
      <FlatList
        data={data}
        renderItem={({ item }) =>
          <View style={styles.standalone}>
            <SwipeRow leftOpenValue={0} rightOpenValue={-75}>
              <View style={styles.standaloneRowBack}>
                <Text style={styles.backTextWhite}></Text>
                <TouchableOpacity onPress={()=>Signout()}>

                <Text style={styles.backTextWhite}>{'Delete'}</Text>
              </TouchableOpacity>

              </View>
              <View style={styles.standaloneRowFront}>
              <Text style={{marginLeft:wp('5%'),fontSize:17,color:'rgba(73, 73, 73, 1)',marginTop:hp('2%')}}>Today 11:55 AM</Text>
     <Text style={{marginLeft:wp('5%'),fontSize:15,color:'rgba(73, 73, 73, 1)',marginTop:hp('1%'),marginRight:wp('5%')}}>the act or an instance of giving notice or information Notification of all winners will occur tomorrow.</Text>
              </View>
            </SwipeRow>
          </View>
        }
      ></FlatList>
    </View>

</View>
   </View>




   </ScrollView>
   
    </View>
  );
}

export default Notification;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    marginTop: 25,
  },
  standalone: {
    marginVertical: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: "black",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 10,
  },
  standaloneRowFront: {
    backgroundColor: 'white',
  },
  standaloneRowBack: {
    alignItems: 'center',
    backgroundColor: 'red',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,

  },
  backTextWhite: {
    color: 'white',
    fontSize:17,
    fontWeight:"bold"
  },
});
