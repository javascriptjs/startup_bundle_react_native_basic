import {Dimensions, StyleSheet, Platform} from 'react-native';
const window = Dimensions.get('window');
import constants from '../../../Constants/Colors';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
const {strings, colors, fonts, urls, PATH} = constants;
const styles = StyleSheet.create({
  backGroundImage: {width: wp('100%')},
  titleTxt: {
    fontWeight: '500',
    fontSize: 25,
    marginTop: hp('10%'),
    width: wp('80%'),
    alignSelf: 'center',

  },
  forgotTxt: {
    color: constants.grey_Text,
    fontWeight: '500',
    fontSize: 12,
    marginTop: hp('3%'),
    width: wp('80%'),
    alignSelf: 'center',
    alignItems: 'center',
    textAlign: 'left',
    marginBottom: hp('3%'),
  },
  flexView: {flexDirection: 'row', alignSelf: 'center'},
  createTxt: {
    color: constants.black_Text,
    fontWeight: '500',
    fontSize: 15,
    marginTop: hp('5%'),
    alignSelf: 'center',
    alignItems: 'center',
    textAlign: 'left',
    marginBottom: hp('5%'),
  },
  signUpTxt: {
    color: constants.grey_Text,
    fontWeight: '500',
    fontSize: 15,
    marginTop: hp('5%'),
    alignSelf: 'center',
    alignItems: 'center',
    textAlign: 'center',
    marginBottom: hp('5%'),
  },
});

export default styles;
