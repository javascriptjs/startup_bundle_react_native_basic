import React, { useState } from 'react';
import {
  Text,
  View,
  Image,
  Alert,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import constants from '../../../Constants/Colors';
import * as Utility from "../../../Utility/index"
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
import Logo from '../../../Components/Logo';
import InputField from '../../../Components/InputField';
import Button from '../../../Components/Buttons';
import styles from './style';
import { Create } from '../../../Redux/Action';
import {useDispatch, useSelector} from 'react-redux';
import Loader from '../../../Components/Loader';

const { strings, colors, fonts, urls, PATH } = constants;
const SignUp = ({ navigation, title }) => {
  const [securityTxtStatus, setSecurityStatus] = useState(true);
  const [securityTxtStatus2, setSecurityStatus2] = useState(true);
  const [email, Setemail] = useState('')
  const [loading, setLoading] = useState(false);
  const [password, SetPassword] = useState('');
  const [ConfirmPassword, SetConfirmPassword] = useState('')
  const [name, Setname] = useState('')
  const [lastName, SetLast] = useState('')
  const dispatch = useDispatch();

  const Register = async () => {
    if (Utility.isFieldEmpty(
      name && email.trim() && password && ConfirmPassword
    )) {
      return (Alert.alert('Please fill all the fields'))
    }
    else if (Utility.isValidEmail(email.trim())) {
      return Alert.alert('Please enter valid email');
    }
    else if (Utility.isValidComparedPassword(password, ConfirmPassword)) {
      return Alert.alert('Password miss match')
    }

    else {
      let body = {
        email: email.trim(),
        password: password,
        firstName: name,
        lastName: lastName,
        deviceToken: "",
        role: ''
      }
      setLoading(true);
      let res = await dispatch(Create(body));
      if (res.statusCode==200) {
        setLoading(false);
        // console.log("email===>",res.email[0])


        setLoading(false);
        Alert.alert(
          '',
          res.message,
          [{text: 'Ok', onPress: () => navigation.navigate('Login')}],
          { 
            cancelable: false
          },
        );

      }
    
      else {
        setLoading(false);
     Alert.alert(res.error)
      }
    }
  }


  return (
    <View style={{ flex: 1 }}>
        <Loader isLoading={loading}></Loader>
      <ScrollView>
      
          <Text style={{fontSize:25,color:constants.grey_Text,textAlign:"center",fontWeight:"900",marginTop:hp('5%')}}>Create an account</Text>

          <InputField
            placeHolder={'Email'}

            autoCapitalization="none"
            inputonChangeText={email => Setemail(email)}
          ></InputField>

          <InputField
          placeHolder={'First Name'}
            inputonChangeText={name => Setname(name)}
          ></InputField>


        <InputField
          placeHolder={'Last Name'}
          inputonChangeText={name => SetLast(name)}
        ></InputField>

          <InputField
            placeHolder={'Password'}
            rightTitle={securityTxtStatus2 == true ? 'eye' : 'eye-slash'}
            securtyTxt={securityTxtStatus2}
            inputonChangeText={password =>SetPassword(password)}

            rightIconPress={() =>
              setSecurityStatus2(!securityTxtStatus2)

            }></InputField>
          <InputField
            placeHolder={'Confirm Password'}
            rightTitle={securityTxtStatus == true ? 'eye' : 'eye-slash'}
            securtyTxt={securityTxtStatus}
            inputonChangeText={ConfirmPassword => SetConfirmPassword(ConfirmPassword)}
            rightIconPress={() =>
              setSecurityStatus(!securityTxtStatus)
            }></InputField>

          <Text style={styles.forgotTxt}>
            By Creating an account you accept the Terms & Condition of the
            Company
          </Text>
          <Button title={'Register'} click={() => Register()}></Button>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <View style={styles.flexView}>
              <Text style={styles.createTxt}>Already a member?</Text>
              <Text style={styles.signUpTxt}>{' Log In'}</Text>
            </View>
          </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default SignUp;
