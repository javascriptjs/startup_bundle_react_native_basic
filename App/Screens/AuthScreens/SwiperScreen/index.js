import React, {useEffect} from 'react';
import {Text, View, Dimensions, TouchableOpacity,StyleSheet,Image} from 'react-native';
import constants from '../../../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
import Swiper from 'react-native-swiper'
import Entypo from "react-native-vector-icons/FontAwesome"
// import messaging from '@react-native-firebase/messaging';

const {strings, colors, fonts, urls, PATH} = constants;
const SwiperScreen = ({navigation}) => {
 
  // const getToken = async () => {
  //   console.log('get token call');
  //   // let fcmToken =await AsyncStorage.read('fcmTocken');
  //   // console.log("get token call fcm token",fcmToken)
  //   // if(!fcmToken){
  //     await messaging().requestPermission();
  //  let  fcmToken = await messaging().getToken();
  //   // if(fcmToken){
  //   console.log('check fcm token', fcmToken);
  //   // await AsyncStorage.save('fcmToken',fcmToken);
  //   // }
  //   // }
  // };
  return (
    <Swiper style={styles.wrapper} showsButtons={false} 
    loop={false}
    activeDot={<View style={{backgroundColor:constants.title_Colors,height:10,borderRadius:10, width:10,marginLeft:10}}
    
    ></View>}
    dot={<View style={{backgroundColor:constants.light_Grey,height:10,width:10,borderRadius:10, marginLeft:10}}
    
    ></View>}>
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: constants.white_Colors,
        flex:1
      }}>
      {/* <View
        style={{
          // height: hp('15%'),
          // width: wp('50%'),
          // backgroundColor: constants.white_Colors,
          // borderRadius: 30 / PixelRatio.get(),
          borderRadius:
            Math.round(
              Dimensions.get('window').width + Dimensions.get('window').height,
            ) / 2,
          width: Dimensions.get('window').width * 0.5,
          height: Dimensions.get('window').width * 0.5,
          backgroundColor: constants.title_Colors,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
       <Text style={{color:constants.white_Colors,fontSize:30,fontWeight:"bold"}}>Logo</Text>
      </View> */}

      <Image source={require('../../../Assets/business.png')} style={{height:hp('28%'),width:wp('50%')}} resizeMode={"contain"}></Image>
      <Text style={{color:constants.black_Text,fontSize:30,fontWeight:"bold",textAlign:"center",marginTop:hp('2%')}}>Startup</Text>
     
      <Text style={{color:constants.black_Text,fontSize:17,textAlign:"center",marginTop:hp('2%'),width:wp('90%'),alignSelf:"center"}}>Manage your health online and get assistance on how to improve your health online with no human interaction.</Text>
    </View>
     
    <View style={styles.slide2}>
    <Image source={require('../../../Assets/business.png')} style={{height:hp('28%'),width:wp('50%')}} resizeMode={"contain"}></Image>
      <Text style={{color:constants.black_Text,fontSize:30,fontWeight:"bold",textAlign:"center",marginTop:hp('2%')}}>Startup</Text>
     
      <Text style={{color:constants.black_Text,fontSize:17,textAlign:"center",marginTop:hp('2%'),width:wp('90%'),alignSelf:"center"}}>Manage your health online and get assistance on how to improve your health online with no human interaction.</Text>
        </View>
        <View style={styles.slide3}>
        <Image source={require('../../../Assets/business.png')} style={{height:hp('28%'),width:wp('50%')}} resizeMode={"contain"}></Image>
      <Text style={{color:constants.black_Text,fontSize:30,fontWeight:"bold",textAlign:"center",marginTop:hp('2%')}}>Startup</Text>
     
      <Text style={{color:constants.black_Text,fontSize:17,textAlign:"center",marginTop:hp('2%'),width:wp('90%'),alignSelf:"center"}}>Manage your health online and get assistance on how to improve your health online with no human interaction.</Text>
         
         <TouchableOpacity onPress={()=>navigation.navigate('Login')}>
         <View style={{height:hp('8%'),
         flexDirection:"row",
         marginTop:hp('5%'),
         
         width:wp('80%'),backgroundColor:constants.title_Colors,borderRadius:50,alignSelf:"center",alignItems:"center",justifyContent:"center"}}>
            <Text style={{fontWeight:"bold",fontSize:20,color:constants.white_Colors}}>Get Started</Text>
         </View>
         </TouchableOpacity>
        </View>
    </Swiper>
  );
};

export default SwiperScreen;

const styles = StyleSheet.create({
  wrapper: {},
  
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:constants.white_Colors
  },
  slide3: {
    alignItems: 'center',
    backgroundColor: constants.white_Colors,
    height:hp('100%'),
    justifyContent:"center"
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold'
  }
})