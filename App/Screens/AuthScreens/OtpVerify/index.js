import React, { useState ,useRef} from 'react';
import { Text, View, ImageBackground, Alert } from 'react-native';
import constants from '../../../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
import Logo from '../../../Components/Logo';
import InputField from '../../../Components/InputField';
import Button from '../../../Components/Buttons';
import styles from './style';
import * as Utility from "../../../Utility/index"
import { ForgotPwd_Api } from '../../../Redux/Action';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../../../Components/Loader'
import OtpInputs from 'react-native-otp-inputs';

const { strings, colors, fonts, urls, PATH } = constants;
const OtpVerfiy = ({ navigation, title }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [email, Setemail] = useState('')
  const otpInput = useRef(null);
  const clearText = () => {
      otpInput.current.clear();
  }

  const setText = () => {
      otpInput.current.setValue("1234");
  }

  const Forgot = async () => {
    // if (Utility.isFieldEmpty(
    //   email.trim()
    // )) {
    //   Alert.alert('', 'Please enter Email');
    // }
    // else if (Utility.isValidEmail(email.trim())) {
    //   Alert.alert('', 'Please Enter Valid Email');
    // }
    // else {
    //   let body = {
    //     email:email.trim()
    //   }
    //   setLoading(true)

    //   let res = await dispatch(ForgotPwd_Api(body))

      
    //    if (res.detail == "Password reset e-mail has been sent.") {
    //     setLoading(false)
    //     Alert.alert(
    //       '',
    //       res.detail,
    //       [{text: 'Ok', onPress: () => navigation.navigate('Login')}],
    //       { 
    //         cancelable: false
    //       },
    //     );
    //   }
    //   else {
    //     setLoading(false)
    //     return Alert.alert("Some thing went wrong")
    //   }
    // }
    navigation.navigate('ResetPassword')
  }
  return (
    <View style={{ flex: 1 }}>
      <Loader isLoading={loading}></Loader>

      <ImageBackground
        source={require('../../../Assets/backGround.png')}
        style={styles.backGroundImage}>
        <Text style={styles.titleTxt}>Otp Verify</Text>
        <OtpInputs
                        handleChange={(code) => console.log(code)}
                        numberOfInputs={4}
                        inputStyles={{ color: "white", textAlign: "center", fontWeight: "800",fontSize:25 }}
                        inputContainerStyles={{ width: 57, backgroundColor: constants.title_Colors, marginLeft: 20 }}
                        underlineColorAndroid={constants.title_Colors}
                        style={{ flexDirection: "row", alignSelf: "center", alignItems: "center", justifyContent: "space-between", marginTop: 40 }}
                    />

        <View style={{ marginTop: hp('10%') }}>
          <Button title={'Submit'} click={() => Forgot()}></Button>
        </View>
      </ImageBackground>
    </View>
  );
};

export default OtpVerfiy;




















