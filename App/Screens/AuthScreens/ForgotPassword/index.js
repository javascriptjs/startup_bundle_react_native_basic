import React, { useState } from 'react';
import { Text, View, ImageBackground, Alert } from 'react-native';
import constants from '../../../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
import Logo from '../../../Components/Logo';
import InputField from '../../../Components/InputField';
import Button from '../../../Components/Buttons';
import styles from './style';
import * as Utility from "../../../Utility/index"
import { ForgotPwd_Api } from '../../../Redux/Action';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../../../Components/Loader'

const { strings, colors, fonts, urls, PATH } = constants;
const ForgotPassword = ({ navigation, title }) => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [email, Setemail] = useState('')


  const Forgot = async () => {
    if (Utility.isFieldEmpty(
      email.trim()
    )) {
      Alert.alert('', 'Please enter Email');
    }
    else if (Utility.isValidEmail(email.trim())) {
      Alert.alert('', 'Please Enter Valid Email');
    }
    else {
      let body = {
        email:email.trim()
      }
      // setLoading(true)

      let res = await dispatch(ForgotPwd_Api(email))

      
       if (res.StatusCode == 200) {
        setLoading(false)
        Alert.alert(
          '',
          res.message,
          [{text: 'Ok', onPress: () =>     navigation.navigate('OtpVerfiy')
        }],
          { 
            cancelable: false
          },
        );
      }
      else {
        setLoading(false)
        return Alert.alert(res.error)
      }
    }


  }
  return (
    <View style={{ flex: 1 }}>
      <Loader isLoading={loading}></Loader>

      <ImageBackground
        source={require('../../../Assets/backGround.png')}
        style={styles.backGroundImage}>
        <Text style={styles.titleTxt}>Forgot Password</Text>
        <InputField
          leftTitle={'envelope'}
          placeHolder={'Email'}

          autoCapitalization="none"
          inputonChangeText={email => Setemail(email)}
        ></InputField>
        <View style={{ marginTop: hp('10%') }}>
          <Button title={'Submit'} click={() => Forgot()}></Button>
        </View>
      </ImageBackground>
    </View>
  );
};

export default ForgotPassword;
