import React, { useState ,useEffect} from 'react';
import { Text, View, ImageBackground, ScrollView, Alert, BackHandler} from 'react-native';
import constants from '../../../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
import * as Utility from "../../../Utility/index"

import Logo from '../../../Components/Logo';
import InputField from '../../../Components/InputField';
import Button from '../../../Components/Buttons';
import styles from './style';
import { Reset_Password } from '../../../Redux/Action';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../../../Components/Loader'
import Header from "../../../Components/Header"
const { strings, colors, fonts, urls, PATH } = constants;
const ChangePassword = ({ navigation, title }) => {
  const dispatch = useDispatch();

  const [securityTxtStatus, setSecurityStatus] = useState(true);
  const [confrimTxtStatus, setConfrimStatus] = useState(true);
  const [OldPassword, SetOldPassword] = useState('');
  const [NewPassword, SetNewPassword] = useState('');
  const [ConfirmPassword, SetConfirmPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const ResetPassword = async () => {

    if (Utility.isFieldEmpty(
      OldPassword && NewPassword && ConfirmPassword
    )) {
      return (Alert.alert('Please fill all the fields'))
    }

    else if (Utility.isValidComparedPassword(NewPassword, ConfirmPassword)) {
      return Alert.alert('Password miss match')
    }

    else {
      let body = {
        oldPassword: OldPassword,
        newPassword: NewPassword,
      }
      setLoading(true)

      let res = await dispatch(Reset_Password(body))
     
    
     if (res.statusCode == 200) {
        setLoading(false)
        Alert.alert(
          '',
          res.message,
          [{ text: 'Ok', onPress: () => navigation.navigate('Login') }],
          {
            cancelable: false
          },
        );
      }
      else {
        setLoading(false)
        return Alert.alert(res.error)

      }
    }

  }
  function handleBackButtonClick() {
    // navigation.goBack();
    console.log("checkback ")
navigation.goBack()
    return true;
  }

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);
  return (
    <View style={{ flex: 1,backgroundColor:'white' }}>
      <Loader isLoading={loading}></Loader>
      <Header navigation={navigation} title={'Change Password'}></Header>
      <ScrollView>
      
<Text style={{fontSize:30,color:constants.grey_Text,textAlign:"center",fontWeight:"900",marginTop:hp('5%')}}></Text>

          <InputField
            leftTitle={'lock'}
            placeHolder={'Old Password'}
            inputonChangeText={OldPassword => SetOldPassword(OldPassword)}
          ></InputField>

          <InputField
            leftTitle={'lock'}
            placeHolder={'New Password'}
            rightTitle={securityTxtStatus == true ? 'eye' : 'eye-slash'}
            securtyTxt={securityTxtStatus}
            inputonChangeText={NewPassword => SetNewPassword(NewPassword)}

            rightIconPress={() =>
              setSecurityStatus(!securityTxtStatus)
            }></InputField>

          <InputField
            leftTitle={'lock'}
            placeHolder={'Confrim Password'}
            rightTitle={confrimTxtStatus == true ? 'eye' : 'eye-slash'}
            securtyTxt={confrimTxtStatus}
            inputonChangeText={ConfirmPassword => SetConfirmPassword(ConfirmPassword)}

            rightIconPress={() =>
              setConfrimStatus(!confrimTxtStatus)
            }></InputField>
          <View style={{ marginTop: hp('10%') }}>
            <Button title={'Save Now!'} click={() => ResetPassword()}></Button>
          </View>
      </ScrollView>
    </View>
  );
};

export default ChangePassword;
