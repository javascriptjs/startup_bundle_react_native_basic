import React, { useState } from 'react';
import { Text, View, ImageBackground, ScrollView, Alert } from 'react-native';
import constants from '../../../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
import * as Utility from "../../../Utility/index"

import Logo from '../../../Components/Logo';
import InputField from '../../../Components/InputField';
import Button from '../../../Components/Buttons';
import styles from './style';
import { Resetpwd } from '../../../Redux/Action';
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../../../Components/Loader'
import Header from "../../../Components/Header"
const { strings, colors, fonts, urls, PATH } = constants;
const ResetPassword = ({ navigation, title }) => {
  const dispatch = useDispatch();

  const [securityTxtStatus, setSecurityStatus] = useState(true);
  const [confrimTxtStatus, setConfrimStatus] = useState(true);
  const [OldPassword, SetOldPassword] = useState('');
  const [NewPassword, SetNewPassword] = useState('');
  const [ConfirmPassword, SetConfirmPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const resetPassword = async () => {

    // if (Utility.isFieldEmpty(
    //   OldPassword && NewPassword && ConfirmPassword
    // )) {
    //   return (Alert.alert('Please fill all the fields'))
    // }

    // else if (Utility.isValidComparedPassword(NewPassword, ConfirmPassword)) {
    //   return Alert.alert('Password miss match')
    // }

    // else {
    //   let body = {
    //     old_password: OldPassword,
    //     new_password1: NewPassword,
    //     new_password2: ConfirmPassword,
    //   }
    //   setLoading(true)

    //   let res = await dispatch(Resetpwd(body))
     
    //   if (res.old_password == "Your old password was entered incorrectly. Please enter it again.") {
    //     setLoading(false)

    //     return Alert.alert(res.old_password[0])
    //   }
    //   else if (res.new_password2) {
    //     setLoading(false);
    //     for (let item of res.new_password2) {
    //       return Alert.alert(item)
    //     }
    //   }
    //   else if (res.detail == "New password has been saved.") {
    //     setLoading(false)
    //     Alert.alert(
    //       '',
    //       res.detail,
    //       [{ text: 'Ok', onPress: () => navigation.navigate('Login') }],
    //       {
    //         cancelable: false
    //       },
    //     );
    //   }
    //   else {
    //     setLoading(false)
    //     return Alert.alert("Something went wrong")

    //   }
    // }
    navigation.navigate('Login')

  }
  return (
    <View style={{ flex: 1 }}>
      <Loader isLoading={loading}></Loader>
      <ScrollView>
        <ImageBackground
          source={require('../../../Assets/backGround.png')}
          style={styles.backGroundImage}>
<Header navigation={navigation}></Header>

          <Text style={styles.titleTxt}>Reset Password</Text>
         

          <InputField
            leftTitle={'lock'}
            placeHolder={'New Password'}
            rightTitle={securityTxtStatus == true ? 'eye' : 'eye-slash'}
            securtyTxt={securityTxtStatus}
            inputonChangeText={NewPassword => SetNewPassword(NewPassword)}

            rightIconPress={() =>
              setSecurityStatus(!securityTxtStatus)
            }></InputField>

          <InputField
            leftTitle={'lock'}
            placeHolder={'Confrim Password'}
            rightTitle={confrimTxtStatus == true ? 'eye' : 'eye-slash'}
            securtyTxt={confrimTxtStatus}
            inputonChangeText={ConfirmPassword => SetConfirmPassword(ConfirmPassword)}

            rightIconPress={() =>
              setConfrimStatus(!confrimTxtStatus)
            }></InputField>
          <View style={{ marginTop: hp('10%') }}>
            <Button title={'Save Now!'} click={() => resetPassword()}></Button>
          </View>
        </ImageBackground>
      </ScrollView>
    </View>
  );
};

export default ResetPassword;
