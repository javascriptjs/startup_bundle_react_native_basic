
import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  Alert,
} from 'react-native';
import constants from '../../../Constants/Colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
import Logo from '../../../Components/Logo';
import InputField from '../../../Components/InputField';
import Button from '../../../Components/Buttons';
import * as Utility from '../../../Utility/index';
import { User_Login, socialLink, facebooksocialLink } from '../../../Redux/Action';
import { useDispatch } from 'react-redux';
import Loader from '../../../Components/Loader';
import styles from './style';
import SyncStorage from 'sync-storage';
const { strings, colors, fonts, urls, PATH } = constants;
const Login = ({ navigation, title }) => {
  const [securityTxtStatus, setSecurityStatus] = useState(true);
  const [email, Setemail] = useState('sahil@gmail.com');
  const [loading, setLoading] = useState(false);
  const [userInfo, setUSerInfo] = useState()
  const [getInfo, gettingLoginStatus] = useState(true)

  const [password, SetPassword] = useState('1234');

const [loginType,setLoginType]=useState('')
  const dispatch = useDispatch();


  // -------------------------facebook login end------------------------
  //  -----------------------simple login---------------------------------------
  const login = async () => {
    if (Utility.isFieldEmpty(email.trim() && password)) {
      return Alert.alert('Please fill all the fields');
    } else if (Utility.isValidEmail(email.trim())) {
      return Alert.alert('Please enter valid email');
    } else {
      let body = {
        email: email.trim(),
        password: password,
      };
      // console.log('login body====>', body);
      setLoading(true);

      let res = await dispatch(User_Login(body));
      console.log("res_login=====>",res)

      if (res.statusCode==200) {
        setLoading(false);
        await Utility.setInLocalStorge('token',res.data.token)
        await Utility.setInLocalStorge('userId', res.data.id)
        await Utility.setInLocalStorge('userInfo', res.data.firstName)
        console.log("check user name", res.data.firstName)
        SyncStorage.set('userInfo', res.data.firstName);
        navigation.navigate('DrawerNavigator');

     
      } else {
        setLoading(false);
Alert.alert(res.error)
      }}
  };
  // --------------------GOOGLE LOGIN--------------------------//
 

  // --------------------------------END GOOGLELOGIN-------------------------//
  return (
    <View style={{}}>
      <Loader isLoading={loading}></Loader>

  
<View style={{marginTop:hp('15%')}}>

  <Text style={{fontSize:30,color:constants.grey_Text,textAlign:"center",fontWeight:"900"}}>Login</Text>
        <InputField
          placeHolder={'Email'}
          autoCapitalization="none"
          inputonChangeText={email => Setemail(email)}></InputField>
        <InputField
          placeHolder={'Password'}
          
          rightTitle={securityTxtStatus == true ? 'eye' : 'eye-slash'}
          securtyTxt={securityTxtStatus}
          inputonChangeText={password => SetPassword(password)}
          rightIconPress={() =>
            setSecurityStatus(!securityTxtStatus)
          }></InputField>

        <Text
          style={styles.forgotTxt}
          onPress={() => navigation.navigate('ForgotPassword')}>
          Forgot Password?
        </Text>
        <Button title={'Continue'} click={() => login()}></Button>
        </View>
      
     

        <View style={styles.flexView}>
          <Text style={styles.createTxt}>Create New Account?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
            <Text style={styles.signUpTxt}>{' SignUp'}</Text>
          </TouchableOpacity>
        </View>

     
    </View>
  );
};

export default Login;
