import {Dimensions, StyleSheet, Platform} from 'react-native';
const window = Dimensions.get('window');
import constants from '../../../Constants/Colors';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from '../../../Utility/index';
const {strings, colors, fonts, urls, PATH} = constants;
const styles = StyleSheet.create({
  backGroundImage: {
    height: hp('100%'),
    width: wp('100%'),
  },
  titleTxt: {
    fontWeight: '500',
    fontSize: 25,
    marginTop: 10,
    width: wp('80%'),
    alignSelf: 'center',
  },
  forgotTxt: {
    color: constants.grey_Text,
    fontWeight: '500',
    fontSize: 17,
    marginTop: 10,
    width: wp('80%'),
    alignSelf: 'center',
    alignItems: 'center',
    textAlign: 'right',
    marginBottom: hp('5%'),
  },
  flexView: {flexDirection: 'row', alignSelf: 'center'},
  createTxt: {
    color: constants.black_Text,
    fontWeight: '500',
    fontSize: 15,
    marginTop: hp('7%'),
    alignSelf: 'center',
    alignItems: 'center',
    textAlign: 'center',
    marginBottom: hp('5%'),
  },
  social:
  {  color: constants.black_Text,
    fontWeight: '500',
    fontSize: 15,
    marginTop: hp('3%'),
    textAlign: 'center',
   },
   social2:
  {  color: constants.facebook,
    fontWeight: "bold",
    fontSize: 15,
    marginTop: hp('3%'),
    textAlign: 'center',
   },
   icon:
   {width:wp("10%"),height:hp("5%"),alignSelf:'center',marginTop:hp("2%")},
   imgicon:
  {width:wp("10%"),height:hp("5%")},
  socailrow:
  {
    width:"35%",
    marginTop: hp("3%"),
    marginBottom:wp("5%"),

    justifyContent:"space-between",
    flexDirection: "row",
    alignSelf: "center"
  },
  signUpTxt: {
    color: constants.grey_Text,
    fontWeight: '500',
    fontSize: 15,
    marginTop: hp('7%'),
    alignSelf: 'center',
    alignItems: 'center',
    textAlign: 'center',
    marginBottom: hp('5%'),
  },
});

export default styles;
