export const BASE_URL = 'http://93.188.167.68:8009/api/';
export const REGISTER = 'users/register';
export const LOGIN = 'users/login';
export const GET_USER_BY_ID = 'users/currentUser/'
export const RESET_PASSWORD = 'users/changePassword/'