import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Alert, Image, StyleSheet } from 'react-native';
import HomeListing from "../Screens/Home"
import PrivacyHome from "../Screens/PrivacyScreens/PrivacyHome"
import constants from '../Constants/Colors';
import Notification from "../Screens/Notification"
import Icon from 'react-native-vector-icons/FontAwesome'
import UserSetting from "../Screens/PrivacyScreens/UserSetting"
const Tab = createBottomTabNavigator();
const BottomTab = () => {
    
    return (
        <Tab.Navigator tabBarOptions={{ activeTintColor: "black" }} screenOptions={{headerShown:false}}>
         
         <Tab.Screen
                name="User Home"
                component={HomeListing}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='user'
                            size={24}
                            color={focused ? constants.title_Colors : '#d9d9d9'}
                        />
                    ),
                })}
               
            />
            <Tab.Screen
                name="Notification"
                component={Notification}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='bell'
                            size={24}
                            color={focused ? constants.title_Colors : '#d9d9d9'}
                        />
                    ),
                })}
               
            />
                <Tab.Screen
                name="User Setting"
                component={UserSetting}
                options={({ route }) => ({
                    tabBarIcon: ({ focused }) => (
                        <Icon
                            name='user'
                            size={24}
                            color={focused ? constants.title_Colors : '#d9d9d9'}
                        />
                    ),
                })}
               
            />
           
        </Tab.Navigator>
    );
};

export default BottomTab;
const styles = StyleSheet.create({

    tabImg2: {
        alignSelf: 'center',
        tintColor: 'grey',
        height: 25,
        width: 30,
    },
    selectedTabImg2: {
        alignSelf: 'center',
        tintColor: 'black',
        height: 25,
        width: 30,
    },

});
