import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Splash from '../Screens/AuthScreens/Splash';
import Login from '../Screens/AuthScreens/Login/Index';
import SignUp from '../Screens/AuthScreens/SignUp';
import ForgotPassword from '../Screens/AuthScreens/ForgotPassword';
import ChangePassword from '../Screens/AuthScreens/ChangePassword';
import OtpVerfiy from "../Screens/AuthScreens/OtpVerify"
import SwiperScreen from '../Screens/AuthScreens/SwiperScreen';
import ResetPassword from '../Screens/AuthScreens/ResetPassword';
import Home from '../Screens/Home';
import Notification from '../Screens/Notification';
import BottomTab from './BottomTab';
import PrivacyHome from "../Screens/PrivacyScreens/PrivacyHome"
import UserSetting from "../Screens/PrivacyScreens/UserSetting"
import PrivacyPolicy from "../Screens/PrivacyScreens/PrivacyPolicy"
import TermCondition from '../Screens/PrivacyScreens/TermCondition';
import ContactUs from "../Screens/PrivacyScreens/ContactUs"
import DrawerNavigator from './DrawerNavigator';
const Stack = createStackNavigator();
const MainStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Splash"
        component={Splash}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SignUp"
        component={SignUp}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="OtpVerfiy"
        component={OtpVerfiy}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="SwiperScreen"
        component={SwiperScreen}
        options={{headerShown: false}}
      />
        <Stack.Screen
        name="ResetPassword"
        component={ResetPassword}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="Notification"
        component={Notification}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="BottomTab"
        component={BottomTab}
        options={{headerShown: false}}
      />
          <Stack.Screen
        name="PrivacyHome"
        component={PrivacyHome}
        options={{headerShown: false}}
      />
        <Stack.Screen
        name="UserSetting"
        component={UserSetting}
        options={{headerShown: false}}
      />
        <Stack.Screen
        name="PrivacyPolicy"
        component={PrivacyPolicy}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="TermCondition"
        component={TermCondition}
        options={{headerShown: false}}
      />
       <Stack.Screen
        name="ContactUs"
        component={ContactUs}
        options={{headerShown: false}}
      />
         <Stack.Screen
        name="DrawerNavigator"
        component={DrawerNavigator}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default MainStackNavigator;
