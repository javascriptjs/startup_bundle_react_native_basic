import React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';
import DrawerData from "../Constants/DrawerData"
import BottomTab from './BottomTab';
const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
    return (
        <Drawer.Navigator
            drawerContent={DrawerData}
            screenOptions={{headerShown:false}}
            drawerContentOptions={{
                activeTintColor: 'black',
            }}>
            <Drawer.Screen
                name="Home"
                component={BottomTab}
                options={{ drawerLabel: 'open drawer' }}
            />
        </Drawer.Navigator>
    );
};

export default DrawerNavigator;
